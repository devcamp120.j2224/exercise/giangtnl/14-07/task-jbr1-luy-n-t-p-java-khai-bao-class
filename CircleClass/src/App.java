public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        double dientich1 = circle1.getArea();
        double dientich2 = circle2.getArea();
        double chuvi1 = circle1.getCircumference();
        double chuvi2 = circle2.getCircumference();
        System.out.println("Diện tích của đối tượng circle1 = " + dientich1 + ", chu vi = " + chuvi1);
        System.out.println("Diện tích của đối tượng circle2 = " + dientich2 + ", chu vi = " + chuvi2);
    }
}
